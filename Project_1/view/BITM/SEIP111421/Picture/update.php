<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP111421\Picture\Picture;
session_start();
$obj_name= new Picture();

if(isset($_FILES['picture'])){
    $errors=array();
    $picture_name=  time().$_FILES['picture']['name'];
    $picture_type=$_FILES['picture']['type'];
    $picture_temp_name=$_FILES['picture']['tmp_name'];
    $picture_size=$_FILES['picture']['size'];
    $test=  explode('.', $picture_name);
    $file_extension=  strtolower(end($test));
    
    $format=array('jpeg','jpg','png');
    
    if(in_array($file_extension, $format)===FALSE){
        $errors[]='Wrong Format';
    }
    if(empty($errors)==TRUE){
        move_uploaded_file($picture_temp_name,"../../../../images/".$picture_name);
        $_POST['picture']=$picture_name;
    }
    $obj_name->prepare($_POST);
    $obj_name->update();
}

