<?php

include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP111421\Hobby\Hobbies;

$hobbyInfo = $_POST['hobby'];
$commaSeparated = implode(',', $hobbyInfo);
$_POST['hobby'] = $commaSeparated;
$hobby = new Hobbies();
$hobby->prepare($_POST);
$hobby->update();
